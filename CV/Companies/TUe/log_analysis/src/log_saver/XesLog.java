package log_saver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import log_analysis.Event;
import log_analysis.Trace;

import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import utils.DB;
import utils.JDomUtil;
import utils.MyFileWriter;

public class XesLog {
	static DB amazonDB = new DB("root", "bpic2012", "mysql",
			"loveyouonly.info", "3306", "bpic2012");
	static Connection connection = amazonDB.getConnection();
	static String logFile = "../BPI_Challenge_2012_cleaned_generated.xes";

	public static void main(String[] args) throws SQLException {
		Element logElement = JDomUtil.getRootElement(logFile);
		logElement.removeChildren("trace", logElement.getNamespace());
		XMLOutputter xml = new XMLOutputter();
		xml.setFormat(Format.getPrettyFormat());

		Statement stmt = connection.createStatement();
		ResultSet rs = stmt
				.executeQuery("select * from 3_gram_lw order by case_id, timestamp");
		rs.next();
		Trace t = new Trace();
		t.reg_date = rs.getTimestamp("case_reg_date");
		t.amountReq = rs.getDouble("amount");
		t.name = rs.getString("case_id");
		t.eventList.add(getEvent(rs));
		int i = 1;
		while (rs.next()) {
			System.out.println(i++);
			// a new case
			if (!t.name.equals(rs.getString("case_id"))) {
				logElement.addContent(t.getElement());
				t = new Trace();
				t.reg_date = rs.getTimestamp("case_reg_date");
				t.amountReq = rs.getDouble("amount");
				t.name = rs.getString("case_id");
			}
			t.eventList.add(getEvent(rs));
		}
		logElement.addContent(t.getElement());
		MyFileWriter.writeToFile("../BPI_Challenge_2012_cleaned__3_gram.xes",
				xml.outputString(logElement.getDocument()), false);
		rs.close();
		stmt.close();
		connection.close();

	}

	private static Event getEvent(ResultSet rs) throws SQLException {
		Event e = new Event();
		e.name = rs.getString("n_gram");
		e.time = rs.getTimestamp("timestamp");
		String lastActivity[] = rs.getString("last_activity").split("-");
		if (lastActivity[lastActivity.length - 1].equals("$")) {
			lastActivity = rs.getString("help").split("-");
		}
		e.transition = lastActivity[lastActivity.length - 1];
		e.resource = rs.getString("resource");
		return e;
	}
}
