package log_analysis;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.jdom2.Element;

import utils.DB;
import utils.JDomUtil;

public class LogAnalyzer {
	static DB amazonDB = new DB("root", "bpic2012", "mysql", "loveyouonly.info",
			"3306", "bpic2012");
	static Connection connection = amazonDB.getConnection();
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		Element rootElement = JDomUtil
				.getRootElement("../BPI_Challenge_2012_cleaned.xes");
		List<Element> traces = rootElement.getChildren("trace",
				rootElement.getNamespace());
		Trace t;
		HashMap<String, Trace> traceEventMap = new HashMap<String, Trace>();
		for (int i = 0; i < traces.size(); i++) {
			t = getTrace(traces.get(i));
			t.insertTraceToDB(connection);
			traceEventMap = processTraceEvents(t.name, traces.get(i),
					traceEventMap);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total processing time: "
				+ ((endTime - startTime) / 1000.0) + " s");
	}

	private static HashMap<String, Trace> processTraceEvents(String traceName,
			Element traceElement, HashMap<String, Trace> traceEventMap) {
		if (traceEventMap.get(traceName) == null) {
			traceEventMap.put(traceName, new Trace(traceName));
		}
		List<Element> eventList = traceElement.getChildren("event",
				traceElement.getNamespace());
		// for every event in the same trace
		Event e;
		for (Element event : eventList) {
			e = getEvent(event);
			e.trace_name = traceName;
			e.insertEvent(connection);
			Trace t = traceEventMap.get(traceName);
			if (t.eventNumMap.get(e.name + " + " + e.transition) == null) {
				t.eventNumMap.put(e.name + " + " + e.transition, 0);
			}
			t.eventNumMap.put(e.name + " + " + e.transition,
					t.eventNumMap.get(e.name + " + " + e.transition) + 1);
			traceEventMap.put(traceName, t);
		}
		return traceEventMap;
	}

	private static Event getEvent(Element event) {
		Event e = new Event();
		List<Element> eventPropEles = event.getChildren("string",
				event.getNamespace());
		e.name = Constant.unknown;
		e.transition = Constant.unknown;
		e.resource = Constant.unknown;
		// for all the properties of this event
		for (Element eventPropEle : eventPropEles) {
			if (eventPropEle.getAttribute("key").getValue()
					.equals("concept:name")) {
				e.name = eventPropEle.getAttribute("value").getValue();
			}
			if (eventPropEle.getAttribute("key").getValue()
					.equals("lifecycle:transition")) {
				e.transition = eventPropEle.getAttribute("value").getValue();
			}
			if (eventPropEle.getAttribute("key").getValue()
					.equals("org:resource")) {
				e.resource = eventPropEle.getAttribute("value").getValue();
			}
		}
		eventPropEles = event.getChildren("date", event.getNamespace());
		for (Element eventPropEle : eventPropEles) {
			if (eventPropEle.getAttribute("key").getValue()
					.equals("time:timestamp")) {
				e.time = Timestamp.valueOf(LocalDateTime.parse(
						eventPropEle.getAttributeValue("value"),
						DateTimeFormatter.ISO_OFFSET_DATE_TIME));
			}
		}
		return e;
	}

	private static Trace getTrace(Element traceElement) {
		Trace t = new Trace();
		List<Element> tracePropEles = traceElement.getChildren("string",
				traceElement.getNamespace());
		for (Element tracePropEle : tracePropEles) {
			if (tracePropEle.getAttribute("key").getValue()
					.equals("concept:name")) {
				t.name = tracePropEle.getAttribute("value").getValue();
			}
			if (tracePropEle.getAttribute("key").getValue()
					.equals("AMOUNT_REQ")) {
				t.amountReq = Double.parseDouble(tracePropEle.getAttribute(
						"value").getValue());
			}
		}
		tracePropEles = traceElement.getChildren("date",
				traceElement.getNamespace());
		for (Element tracePropEle : tracePropEles) {
			if (tracePropEle.getAttribute("key").getValue().equals("REG_DATE")) {
				t.reg_date = Timestamp.valueOf(LocalDateTime.parse(
						tracePropEle.getAttributeValue("value"),
						DateTimeFormatter.ISO_OFFSET_DATE_TIME));
			}
		}
		return t;
	}
}
