package log_analysis;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.Namespace;

import utils.DB;

public class Trace {
	public Trace() {
	}

	public Trace(String name) {
		this.name = name;
	}

	public double amountReq;
	public String name = " ";
	public Timestamp reg_date;
	public HashMap<String, Integer> eventNumMap = new HashMap<String, Integer>();
	public List<Event> eventList = new ArrayList<Event>();

	public void insertTraceToDB(Connection connection) {
		while (true) {
			try {
				PreparedStatement stmt = connection
						.prepareStatement("insert into trace (name, amount_req, reg_date) values (?, ?, ?)");
				stmt.setString(1, name);
				stmt.setDouble(2, amountReq);
				stmt.setTimestamp(3, reg_date);
				stmt.executeUpdate();
				stmt.close();
				return;
			} catch (SQLException e) {
				System.out
						.println("lost connection when inserting trace, retry after 5 sec");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				DB amazonDB = new DB("root", "bpic2012", "mysql",
						"loveyouonly.info", "3306", "bpic2012");
				connection = amazonDB.getConnection();
			}
		}
	}

	public Element getElement() {
		Namespace ns = Namespace.getNamespace("http://www.xes-standard.org/");
		Element e = new Element("trace", ns);
		addAttribute(e, "date", "REG_DATE",
				reg_date.toString().replaceFirst(" ", "T"));
		addAttribute(e, "string", "AMOUNT_REQ", String.valueOf(amountReq));
		addAttribute(e, "string", "concept:name", name);

		for (Event event : eventList) {
			Element eventElement = new Element("event", ns);
			addAttribute(eventElement, "string", "concept:name", event.name);
			addAttribute(eventElement, "date", "time:timestamp", event.time
					.toString().replaceFirst(" ", "T"));
			addAttribute(eventElement, "string", "lifecycle:transition",
					"COMPLETE");
			if (!event.resource.equals("UNKNOWN")) {
				addAttribute(eventElement, "string", "org:resource",
						event.resource);
			}
			e.addContent(eventElement);
		}

		return e;
	}

	private void addAttribute(Element ele, String type, String key, String value) {
		Element attr = new Element(type, ele.getNamespace());
		Attribute attrKey = new Attribute("key", key);
		Attribute attrValue = new Attribute("value", value);
		attr.setAttribute(attrKey);
		attr.setAttribute(attrValue);
		ele.addContent(attr);
	}
}