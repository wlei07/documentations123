package log_analysis;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import utils.DB;

public class Event {
	public String name;
	public String transition;
	public String trace_name;
	public String resource;
	public Timestamp time;

	public void insertEvent(Connection connection) {
		while (true) {
			try {
				PreparedStatement stmt = connection
						.prepareStatement("insert into event (name, transition, trace_name, timestamp, resource)"
								+ " values (?, ?, ?, ?, ?)");
				stmt.setString(1, name);
				stmt.setString(2, transition);
				stmt.setString(3, trace_name);
				stmt.setTimestamp(4, time);
				stmt.setString(5, resource);
				stmt.executeUpdate();
				stmt.close();
				return;
			} catch (SQLException e) {
				System.out.println("exception during event insertion: "
						+ e.getMessage() + " retry after 5s...");
				DB amazonDB = new DB("root", "bpic2012", "mysql",
						"loveyouonly.info", "3306", "bpic2012");
				connection = amazonDB.getConnection();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}