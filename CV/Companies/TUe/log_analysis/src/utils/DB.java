package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB {
	public DB(String username, String password, String protocol, String server,
			String port, String database) {
		super();
		this.username = username;
		this.password = password;
		this.protocol = protocol;
		this.server = server;
		this.port = port;
		this.database = database;
	}

	String username;
	String password;
	String protocol;
	String server;
	String port;
	String database;

	public Connection getConnection() {
		while (true) {
			try {
				return DriverManager.getConnection("jdbc:" + protocol + "://"
						+ server + ":" + port + "/" + database, username,
						password);
			} catch (SQLException e) {
				System.out.println("exception: " + e.getMessage() + " retry after 5s...");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
