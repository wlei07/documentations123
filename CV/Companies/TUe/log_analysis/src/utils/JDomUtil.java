package utils;

import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class JDomUtil {
	public static SAXBuilder jdomBuilder = new SAXBuilder();

	public static Element getRootElement(String file) {
		Document log = null;
		try {
			log = jdomBuilder.build(file);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Element rootElement = log.getRootElement();
		return rootElement;
	}
}
