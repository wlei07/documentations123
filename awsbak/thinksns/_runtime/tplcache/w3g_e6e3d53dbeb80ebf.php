<?php if (!defined('THINK_PATH')) exit();?><div class="cob">
	<div class="c" id="c_<?php echo ($feed[feed_id]); ?>" cid="<?php echo ($feed[feed_id]); ?>" rowid="<?php echo ($feed[feed_id]); ?>" appid="<?php echo ($feed[uid]); ?>">
		<!-- 判断是否被收藏 -->
		<?php if($feed['iscoll']['colled']==1){ ?>
			<div class="sc sc_1" id="sc_<?php echo ($feed[feed_id]); ?>" cid="<?php echo ($feed[feed_id]); ?>" type="<?php echo ($feed["app_row_table"]); ?>"></div>
		<?php }else{ ?>
			<div class="sc" id="sc_<?php echo ($feed[feed_id]); ?>" cid="<?php echo ($feed[feed_id]); ?>" type="<?php echo ($feed["app_row_table"]); ?>"></div>
		<?php } ?>

		<!-- 原微博 -->
		<div class="c_info">
			<div class="c_ava">
				<img src="<?php echo ($feed["avatar_small"]); ?>" width=40 height=40>
			</div>
			<div class="info_text">
				<div class="c_info_name"><?php echo ($feed[uname]); ?></div>
				<div class="c_info_more_box">
					<div class="c_time"><?php echo (friendlydate($feed[publish_time])); ?></div>
					<div id="c_digg_count_<?php echo ($feed["feed_id"]); ?>" class="c_digg_count">赞：<?php echo ($feed["digg_count"]); ?></div>
					<div class="c_zf_count">转发：<?php echo ($feed[repost_count]); ?></div>
					<div class="c_comment_count">评论：<?php echo ($feed[comment_count]); ?></div>
				</div>
			</div>
		</div>
		<div class="c_content">
			<?php echo wapFormatContent($feed[content]) ?>
		</div>
		<!-- 附件 -->
			<div><?php if(isset($feed['attach']) && $feed['type'] == 'postfile'){ ?>
				<div class="attach_box">
					<?php foreach($feed['attach'] as $value){
							switch($value['extension']){
								case 'png':$type='pic';break;
								case 'jpg':$type='pic';break;
								case 'jpeg':$type='pic';break;
								case 'bmp':$type='pic';break;
								case 'gif':$type='pic';break;
								case 'zip':$type='zip';break;
								case 'rar':$type='zip';break;
								case 'doc':$type='word';break;
								case 'docx':$type='word';break;
								case 'xls':$type='xls';break;
								case 'ppt':$type='ppt';break;
								case 'pdf':$type='pdf';break;
								default:$type='unknow';break;
							}
							$size=$value['size'];
							if($size > 1024 && $size< 1024*1024){
								$size=$size/1024;
								$size=round($size,2);
								$size.='K';
							}else if($size < 1024){
								$size.='B';
							}else{
								$size=$size/1024/1024;
								$size=round($size,2);
								$size.='M';
							} ?>
						<a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
							<div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
								<div class="attachs_name">
									<?php echo $value['attach_name']; ?>
								</div>
								<div class="attachs_size">
									<?php echo $size; ?>
								</div>
							</div>
						</a>
					<?php } ?>
				</div>
			  <?php } ?>
			</div>
		<!-- 原微博的图 -->
		<?php if($feed['feedType']=='postimage') { ?>
		<div class="feed_img_box">
			<?php foreach($feed['attach'] as $value){ ?>
				<img class="feed_img" bm="<?php echo ($value['attach_middle']); ?>" src="<?php echo ($value['attach_small']); ?>" linkto="<?php echo U('w3g/Index/detail',array('weibo_id'=>$feed['feed_id']));?>">
			<?php } ?>
		</div>
		<?php } ?>

		<!-- 转发 -->
		<?php if ($feed['type'] == 'repost') { ?>
		<div class="c_zf_box">
			<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
			<div class="c_zf_content">
				<span class="c_zf_content_name">@<?php echo ($feed[transpond_data][uname]); ?></span>:
				<?php echo wapFormatContent($feed['transpond_data'][feed_content]) ?>
			</div>
		<!-- 转发的图 -->
		<?php if($feed['transpond_data']['type']=='postimage') { ?>
			<div class="feed_img_box">
				<?php foreach($feed['transpond_data']['attach'] as $value){ ?>
					<img class="feed_img" src="<?php echo ($value['attach_small']); ?>" linkto="<?php echo U('w3g/Index/detail',array('weibo_id'=>$feed['weibo_id']));?>">
				<?php } ?>
			</div>
		<?php } ?>
			<div class="c_zf_info">
				<div class="c_zf_info_time"><?php echo (friendlydate($feed[transpond_data][publish_time])); ?></div>
				<div class="c_zf_info_counts">
					<span class="c_zf_info_count">转发:<?php echo ($feed[transpond_data][repost_count]); ?></span>
					<span class="c_zf_info_count">评论:<?php echo ($feed[transpond_data][comment_count]); ?></span>
					<span class="c_zf_info_count">赞:<?php echo ($feed[transpond_data][digg_count]); ?></span>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="c_d">
		<div class="c_d_yes">
			<div class="c_d_text">
				删除
			</div>
		</div>
		<div class="c_d_no">
			<div class="c_d_text">
				取消
			</div>
		</div>
	</div>
</div>