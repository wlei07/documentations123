<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/> 
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title> <?php echo ($headtitle); ?></title>
	<base href="__APP__/" />
	<script type="text/javascript" src="js/l_l.min.js"></script>
	<script type="text/javascript">
		var SITE_URL  = '<?php echo SITE_URL; ?>';
		//载入函数
	  	var U = function(url, params) {
		var website = SITE_URL+'/index.php';
		url = url.split('/');
		if(url[0]=='' || url[0]=='@')
			url[0] = APPNAME;
		if (!url[1])
			url[1] = 'Index';
		if (!url[2])
			url[2] = 'index';
		website = website+'?app='+url[0]+'&mod='+url[1]+'&act='+url[2];
		if(params) {
			params = params.join('&');
			website = website + '&' + params;
		}
		return website;
		};
		LazyLoad.js(["js/zepto.min.js", "js/z.touch.js","js/z.ajax.js","js/z.r.js?v=<?php echo ($site["sys_version"]); ?>","js/z.js?v=<?php echo ($site["sys_version"]); ?>"], function(){
			console.log('%cThinkSNS %c3G版\n%cwww.thinksns.com','color:#2980B9;font-size: 28px;','color:#1ABC9C; font-size: 28px;','color:#2980B9');
		});
	</script>
	<link rel="stylesheet" href="css/touch.css?v=<?php echo ($site["sys_version"]); ?>">
	<link rel="stylesheet" href="css/demo.css?v=<?php echo ($site["sys_version"]); ?>">
</head>

<body uid="<?php echo ($_SESSION['mid']); ?>">
<!-- 所有菜单 -->
	<div id="menu_box">
		<div id="default_menu_box_option">
			<!-- <div id="open_write_twitter" class="menu_box_option">写微博</div> -->
			<div id="open_hf_twitter" class="menu_box_option">回复</div>
			<div id="open_zf_twitter" class="menu_box_option">转发</div>
			<div id="open_yw" class="menu_box_option">详情</div>
			<div id="open_zz" class="menu_box_option">作者</div>
			<div id="digit" class="menu_box_option">赞</div>
		</div>
		<div id="more_menu_box_option">
			<div id="open_more" class="menu_box_option">更多</div>
			<!-- <div id="open_menu" class="menu_box_option">菜单</div> -->
			<div id="open_square" class="menu_box_option">广场</div>
			<div id="back2_default_menu_box_option" class="menu_box_option">返回</div>
		</div>
	</div>
	<!-- 微博输入框 -->
	<!-- //发布微博限制字数 -->
	<?php $admin_Config = model('Xdata')->lget('admin_Config');
		$weibo_nums = $admin_Config['feed']['weibo_nums']; ?>
	<div id="post_twitter">
		<div class="header">
			<div id="post_tip" class="logo_post">发表微博</div>
			<!-- <input type="button" id="post_twitter_button_file" value="附件" tabindex="6"> -->
			<div id="post_twitter_button_file"></div>
			<div id="ifSorC">
				<div id="ifShareFeed" class="no_check">转发到微博</div>
				<div id="ifAsComment" class="no_check">评论给原作者</div>
			</div>
		</div>
	   	<div id="ptibox" nums="<?php echo ($weibo_nums); ?>">
			<form id="post_form" type="fb" action="<?php echo U('w3g/Index/doComment');?>" method="post" enctype="multipart/form-data" ava="<?php echo ($profile); ?>"/>
				<input type="hidden" name="feed_id" value="<?php echo ($_GET['feed_id']); ?>" />
	   			<input type="hidden" name="comment_id" value="<?php echo ($_GET['comment_id']); ?>"/>
				<textarea name="content" id="post_twitter_input"></textarea>
			</form>
	   	</div>
   		<div id="file_list"></div>
		<div id="post_twitter_button">
			<input type="button" id="close_post_twitter" value="取消" tabindex="6">
			<input type="button" id="post_twitter_button_submit" value="发表" tabindex="6">
		</div>
	</div>
	<!-- 发表私信 -->
	<div id="post_message" style="display:none;">
		<div class="header">
			<div id="post_message_tip" class="logo_post">发送私信</div>
		</div>
	   	<div id="pmbox">
			<!-- <div id="post_message_tip">发送私信：</div> -->
				<input id="post_msg_to" type="text" placeholder="发送给"></input>
				<div id="pm2l"></div>
				<textarea name="content" id="post_message_input" placeholder="私信内容"></textarea>
	   	</div>
			<div id="post_message_button">
				<input type="button" id="close_post_message" value="取消" tabindex="6">
				<input type="button" id="post_message_button_submit" value="发表" tabindex="6">
			</div>
	</div>
	<!-- 收藏提示 -->
	<div id="favorited"></div>
	<!-- 右上角菜单 -->
	<div id="sys_menu" class="sys_menu_normal">
		<div id="sys_menu_top"></div>
		<div id="sys_menu_wtire" class="sys_menu_option">写微博</div>
		<div id="sys_menu_i" class="sys_menu_option">我的主页</div>
		<div id="sys_menu_msg" class="sys_menu_option" linkto="at">消息盒子</div>
		<div id="sys_menu_square" class="sys_menu_option">微博广场</div>
		<div id="sys_menu_sendmsg" class="sys_menu_option">发私信</div>
		<div id="sys_menu_search" class="sys_menu_option">搜索</div>
		<div id="sys_menu_quit" class="sys_menu_option">退出</div>
	</div>
	<!-- 局部遮罩 -->
	<div id="part_shadow"></div>
	<!-- 全局遮罩 -->
	<div id="shadow"></div>
	<div id="no"></div>
	<div id="tip">
		<img id="tip_load" src="img/link2load.gif">
		<p id="tip_p" class="tip_p_fl">
			Here is ThinkSNS-3G tips:)
		</p>
		<div id="tip_ik">
			<p id="tip_ikp">
				我知道了
			</p>
		</div>
	</div>
	<div id="tip_shadow"></div>
	<!-- 头部顶栏 -->
	<div id="header">
		<div id="logo"
			 <?php if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE 6.0') !== false): ?>style="_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo ($site["logo"]); ?>', sizingMethod='crop');_background:none;"<?php else: ?>style="background:url(<?php echo ($site["logo"]); ?>) no-repeat;"<?php endif; ?>
		></div>
		<div id="menu_button" class="menu_button_normal"></div>
	</div>
	<!-- 加载提示 -->
	<div id="load_tip">
		<img id="load_tip_img" src="img/load_tip.gif">
	</div>
	<!-- 查看图片大图 -->
	<div id="feed_img_view">
		<div id="feed_img_view_bar">
			<div id="feed_img_view_close"></div>
			<div id="feed_img_view_zan"></div>
		</div>
		<div id="feed_img_view_img_box">
		</div>
		<div id="feed_img_view_bg"></div>
	</div>
	<!-- 用户消息提醒 -->
	<div id="msg_tip_box">
		<div id="msg_tip">
			<p id="msg_tip_p">0</p>
			<img id="msg_tip_img" src="img/msg_tip.png">
		</div>
		<div id="msg_tip_shadow"></div>
	</div>
	<!-- 搜索弹出层  -->
<div id="search_box">
	<div class="header">
		<div id="post_message_tip" class="logo_post" style="height: 20px;">搜索</div>
	</div>
	<div id="search_sort">
		<div id="sscw" class="ssc ssc_check" stype="weibo">微博</div>
		<div id="sscu" class="ssc" stype="user">用户</div>
		<div id="ssct" class="ssc" stype="">话题</div>
	</div>
	<div id="search_input_box">
		<input type="text" name="sibi" id="sibi">
	</div>
	<div id="search_button">
		<input type="button" id="close_search_box" value="取消" tabindex="6">
		<input type="button" id="post_search_submit" value="搜索" tabindex="6">
	</div>
</div>
<div id="refresh_list">
	<p id="refresh_list_p">下拉可以刷新</p>
</div>
<!-- 主体内容 -->
<div id="content"  msgpage="replyMe" interface="list">
<div id="msg_nav" act="at">
	<div id="mag_nav_msg" class="msg_navs" linkto="msg">
		<div class="msg_nav_title">私信</div>
	</div>
	<div id="mag_nav_at" class="msg_navs" linkto="at">
		<div class="msg_nav_title">@我</div>
	</div>
	<div id="mag_nav_cm" class="msg_navs" linkto="cm">
		<div class="msg_nav_title">评论我</div>
	</div>
	<div id="mag_nav_notify" class="msg_navs" linkto="notify">
		<div class="msg_nav_title">通知</div>
	</div>
</div>
<?php if(empty($commentlist)) { ?>暂无更多信息<?php } ?>
	<?php if(is_array($commentlist)): ?><?php $i = 0;?><?php $__LIST__ = $commentlist?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$feed): ?><?php ++$i;?><?php $mod = ($i % 2 )?><div id="c_<?php echo ($feed["sourceInfo"]["feed_id"]); ?>" class="c_replyme" cid="<?php echo ($feed["sourceInfo"]["feed_id"]); ?>" commentid="<?php echo ($feed["comment_id"]); ?>" rowid="<?php echo ($feed["sourceInfo"]["feed_id"]); ?>" appid="<?php echo ($feed["uid"]); ?>" touid="<?php echo ($feed["uid"]); ?>" page="" feedtype="<?php echo ($feed["sourceInfo"]["type"]); ?>" isdel="<?php echo ($feed["sourceInfo"]["api_source"]["is_del"]); ?>">
		

		<!-- 评论 -->
		<div class="c_info">
			<div class="c_ava">
				<img src="<?php echo ($feed["user_info"]["avatar_small"]); ?>" width=40 height=40>
			</div>
			<div class="info_text">
				<div id="c_info_name_<?php echo ($feed["sourceInfo"]["feed_id"]); ?>" class="c_info_name"><?php echo ($feed["user_info"]["uname"]); ?></div>
				<div class="c_info_more_box">
					<div class="c_time">
						<?php if($feed['app'] == 'weiba' || $feed['app'] == 'w3g' || $feed['app'] == 'public'){
								$ctime=strtotime($feed[ctime]);
								echo friendlyDate($ctime);
							}else{
								echo friendlyDate($feed[ctime]);
							} ?>
					</div>
				</div>
			</div>
		</div>
		<div id="c_content_<?php echo ($feed["sourceInfo"]["feed_id"]); ?>" class="c_content">
			<?php if($feed[to_comment_id] != 0){
					$res = M('Comment')->where('comment_id='.$feed[to_comment_id])->find();
					$uname = getUserName($feed[to_uid]);
					echo wapFormatContent($feed[content].' //@'.$uname.'：'.$res[content]);
				}else{
					echo wapFormatContent($feed[content]);
				} ?>
		</div>

		<!-- 原文 -->

		<div class="c_zf_box">
			<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
			<?php if($feed[sourceInfo][is_del] == 0){ ?>
				<div class="c_zf_content">
					<div id="c_content_yw_<?php echo ($feed["sourceInfo"]["feed_id"]); ?>">
						<span class="c_zf_content_name">@<?php echo ($feed["sourceInfo"]["source_user_info"]["uname"]); ?></span>:
						<?php echo wapFormatContent($feed['sourceInfo'][feed_content]); ?>
					</div>
					<?php if($feed['sourceInfo']['type'] == 'weiba_post' || $feed['sourceInfo']['source_table'] == 'weiba_post'){ ?>
								<div class="loadweiba" weibaid="<?php echo ($feed["sourceInfo"]["api_source"]["post_id"]); ?>">
									查看全文
								</div>
							<?php } ?>
				</div>
				<!-- 原文的图 -->
				<?php if($feed['sourceInfo']['type']=='postimage') { ?>
					<div class="feed_img_box">
						<?php foreach($feed['sourceInfo']['attach'] as $value){ ?>
							<img bm="<?php echo ($value['attach_middle']); ?>" class="feed_img" src="<?php echo ($value['attach_small']); ?>">
						<?php } ?>
					</div>
				<?php } ?>
			<?php }else{ ?>
				<!-- 已被删除 -->
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">原文已被删除</div>

			<?php } ?>
		<!-- 转发的原文 -->
		<?php if (($feed['sourceInfo']['type'] == 'repost' && $feed['sourceInfo']['api_source']['is_del'] == '0') || ($feed['sourceInfo']['type'] == 'repost' && $feed['transpond_data']['api_source']['is_del'] == '0') || ($feed['sourceInfo']['type'] == 'repost' && $feed['sourceInfo']['is_del'] == '0')) { ?>
			<div id="c_zf_box_<?php echo ($feed["feed_id"]); ?>" class="c_zf_box">
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">
					<span class="c_zf_content_name">@<?php echo ($feed["sourceInfo"]["transpond_data"]["uname"]); ?></span>:
					<?php echo wapFormatContent($feed['sourceInfo']['transpond_data'][feed_content]);
						if($feed['sourceInfo']['transpond_data']['type'] == 'weiba_post'){ ?>
							<div class="loadweiba" weibaid="<?php echo ($feed["sourceInfo"]["transpond_data"]["api_source"]["post_id"]); ?>">
								查看全文
							</div>
						<?php } ?>
				</div>
			<!-- 转发的图 -->
			<?php if($feed['sourceInfo']['transpond_data']['type']=='postimage') { ?>
				<div class="feed_img_box">
					<?php foreach($feed['sourceInfo']['transpond_data']['attach'] as $value){ ?>
						<img bm="<?php echo ($value['attach_middle']); ?>" class="feed_img" src="<?php echo ($value['attach_small']); ?>">
					<?php } ?>
				</div>
			<?php } ?>

			<!-- 附件 -->
			<div ><?php if(isset($feed['sourceInfo']['transpond_data']['attach']) && $feed['sourceInfo']['transpond_data']['type'] == 'postfile'){ ?>
				<div class="attach_box">
					<?php foreach($feed['sourceInfo']['transpond_data']['attach'] as $value){
							switch($value['extension']){
								case 'png':$type='pic';break;
								case 'jpg':$type='pic';break;
								case 'jpeg':$type='pic';break;
								case 'bmp':$type='pic';break;
								case 'gif':$type='pic';break;
								case 'zip':$type='zip';break;
								case 'rar':$type='zip';break;
								case 'doc':$type='word';break;
								case 'docx':$type='word';break;
								case 'xls':$type='xls';break;
								case 'ppt':$type='ppt';break;
								case 'pdf':$type='pdf';break;
								default:$type='unknow';break;
							}
							$size=$value['size'];
							if($size > 1024 && $size< 1024*1024){
								$size=$size/1024;
								$size=round($size,2);
								$size.='K';
							}else if($size < 1024){
								$size.='B';
							}else{
								$size=$size/1024/1024;
								$size=round($size,2);
								$size.='M';
							} ?>
						<a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
							<div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
								<div class="attachs_name">
									<?php echo $value['attach_name']; ?>
								</div>
								<div class="attachs_size">
									<?php echo $size; ?>
								</div>
							</div>
						</a>
					<?php } ?>
				</div>
			  <?php } ?>
			</div>
				<div class="c_zf_info">
					<div class="c_zf_info_time"><?php echo (friendlydate($feed["sourceInfo"]["transpond_data"]["publish_time"])); ?></div>
					<div class="c_zf_info_counts">
						<span class="c_zf_info_count">转发:<?php echo ($feed["sourceInfo"]["transpond_data"]["repost_count"]); ?></span>
						<span class="c_zf_info_count">评论:<?php echo ($feed["sourceInfo"]["transpond_data"]["comment_count"]); ?></span>
						<span class="c_zf_info_count">赞:<?php echo ($feed["sourceInfo"]["transpond_data"]["digg_count"]); ?></span>
					</div>
				</div>
			</div>
		<?php }elseif (($feed['sourceInfo']['type'] == 'repost' && $feed['sourceInfo']['api_source']['is_del'] == '1') || ($feed['type']['sourceInfo'] == 'repost' && $feed['transpond_data']['api_source']['is_del'] == '1') || ($feed['sourceInfo']['type'] == 'repost' && $feed['sourceInfo']['is_del'] == '1')) { ?>
			<!-- 原微博已被删除 -->
			<div class="c_zf_box">
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">原文已被删除</div>
			</div>
		<?php } ?>
		<!--转发原文结束-->
		
		<!-- 原文的附件 -->
		<div ><?php if(isset($feed['sourceInfo']['attach']) && $feed['sourceInfo']['type'] == 'postfile'){ ?>
					<div class="attach_box">
						<?php foreach($feed['sourceInfo']['attach'] as $value){
								switch($value['extension']){
									case 'png':$type='pic';break;
									case 'jpg':$type='pic';break;
									case 'jpeg':$type='pic';break;
									case 'bmp':$type='pic';break;
									case 'gif':$type='pic';break;
									case 'zip':$type='zip';break;
									case 'rar':$type='zip';break;
									case 'doc':$type='word';break;
									case 'docx':$type='word';break;
									case 'xls':$type='xls';break;
									case 'ppt':$type='ppt';break;
									case 'pdf':$type='pdf';break;
									default:$type='unknow';break;
								}
								$size=$value['size'];
								if($size > 1024 && $size< 1024*1024){
									$size=$size/1024;
									$size=round($size,2);
									$size.='K';
								}else if($size < 1024){
									$size.='B';
								}else{
									$size=$size/1024/1024;
									$size=round($size,2);
									$size.='M';
								} ?>
							<a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
								<div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
									<div class="attachs_name">
										<?php echo $value['attach_name']; ?>
									</div>
									<div class="attachs_size">
										<?php echo $size; ?>
									</div>
								</div>
							</a>
						<?php } ?>
					</div>
			  <?php } ?>
		</div>

		<!--微吧 转发的原文 -->
		<?php if ($feed['sourceInfo']['type'] == 'weiba_repost' && $feed['sourceInfo']['api_source']['is_del'] == '0' || ($feed['sourceInfo']['source_table'] == 'weiba_repost' && $feed['api_source']['is_del'] == '0')){ ?>
			<div id="c_zf_box_<?php echo ($feed["feed_id"]); ?>" class="c_zf_box">
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">
					<span class="c_zf_content_name">@<?php echo ($feed["sourceInfo"]["api_source"]["source_user_info"]["uname"]); ?></span>:
					<?php echo wapFormatContent($feed['sourceInfo']['api_source']['source_content']); ?>
							<div class="loadweiba" weibaid="<?php echo ($feed["sourceInfo"]["api_source"]["post_id"]); ?>">
								查看全文
							</div>
				</div>
				<div class="c_zf_info">
					<div class="c_zf_info_time"><?php echo (friendlydate($feed["sourceInfo"]["transpond_data"]["publish_time"])); ?></div>
					<div class="c_zf_info_counts">
						<span class="c_zf_info_count">回复:<?php echo ($feed["sourceInfo"]["api_source"]["reply_count"]); ?></span>
						<span class="c_zf_info_count">浏览:<?php echo ($feed["sourceInfo"]["api_source"]["read_count"]); ?></span>
					</div>
				</div>
			</div>
		<?php }elseif($feed['sourceInfo']['type'] == 'weiba_repost' && $feed['sourceInfo']['api_source']['is_del'] == '1' || ($feed['sourceInfo']['source_table'] == 'weiba_repost' && $feed['api_source']['is_del'] == '1')){ ?>
			<!-- 原微博已被删除 -->
			<div class="c_zf_box">
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">原文已被删除</div>
			</div>
		<?php } ?>


		</div>
		<!-- 原文结束 -->
	</div><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>

	<!-- 分页 -->
	<div id="pager_box">
		<?php if(empty($_GET['page']) || $_GET['page']==1){ ?>
			<a href="javascript:;" link="" id="prev" class="pager_child pager_p_n_none ">上一页</a>
		<?php }else{ ?>
			<a href="javascript:;" link="<?php echo U('w3g/Index/replyMe',array('page'=>$page-1));?>" id="prev" class="pager_child pager_p_n ">上一页</a>
		<?php } ?>
		<select id="page_sel" class="pager_child">
			<?php $_GET['page']!=0?$get=$_GET['page']:$get=1;
				$_count = ceil($count/10);
				if($get<6){
					for($i=0;$i<($_count);$i++){
						$get==$i+1?$selected='selected="selected"':$selected='';
						if($i>9){
							break;
						} ?>
			<option value="<?php echo U('w3g/Index/replyMe');?>&page=<?php echo ($i+1); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
			<?php }
				}elseif($get>=6){
					for($i=($get-5);$i<$get;$i++){
						$get==$i+1?$selected='selected="selected"':$selected=''; ?>
			<option value="<?php echo U('w3g/Index/replyMe');?>&page=<?php echo ($i+1); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
			<?php } ?>
			<?php for($i=$get;$i<($get+5);$i++){
						$get==$i+1?$selected='selected="selected"':$selected='';
						if($i>$_count-1){
							break;
						} ?>
			<option value="<?php echo U('w3g/Index/replyMe');?>&page=<?php echo ($i+1); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
			<?php } ?>
			<?php } ?>
			
		</select>
		<?php if($get == ($_count)) { ?>
			<a href="javascript:;" link="" id="next" class="pager_child pager_p_n_none">下一页</a>
		<?php }else{ ?>
			<a href="javascript:;" link="<?php echo U('w3g/Index/replyMe',array('page'=>$page+1));?>" id="next" class="pager_child pager_p_n">下一页</a>
		<?php } ?>
	</div>
</div>
	<!-- 列表结束################################################################################################ -->
	<!-- footer -->
	<div id="footer">
		<p id="footer_p">©ThinkSNS.com  2013</p>
	</div>
</div>
</body>
</html>