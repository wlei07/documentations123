<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/> 
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title> <?php echo ($headtitle); ?></title>
	<base href="__APP__/" />
	<script type="text/javascript" src="js/l_l.min.js"></script>
	<script type="text/javascript">
		var SITE_URL  = '<?php echo SITE_URL; ?>';
		//载入函数
	  	var U = function(url, params) {
		var website = SITE_URL+'/index.php';
		url = url.split('/');
		if(url[0]=='' || url[0]=='@')
			url[0] = APPNAME;
		if (!url[1])
			url[1] = 'Index';
		if (!url[2])
			url[2] = 'index';
		website = website+'?app='+url[0]+'&mod='+url[1]+'&act='+url[2];
		if(params) {
			params = params.join('&');
			website = website + '&' + params;
		}
		return website;
		};
		LazyLoad.js(["js/zepto.min.js", "js/z.touch.js","js/z.ajax.js","js/z.r.js?v=<?php echo ($site["sys_version"]); ?>","js/z.js?v=<?php echo ($site["sys_version"]); ?>"], function(){
			console.log('%cThinkSNS %c3G版\n%cwww.thinksns.com','color:#2980B9;font-size: 28px;','color:#1ABC9C; font-size: 28px;','color:#2980B9');
		});
	</script>
	<link rel="stylesheet" href="css/touch.css?v=<?php echo ($site["sys_version"]); ?>">
	<link rel="stylesheet" href="css/demo.css?v=<?php echo ($site["sys_version"]); ?>">
</head>

<body uid="<?php echo ($_SESSION['mid']); ?>">
<!-- 所有菜单 -->
	<div id="menu_box">
		<div id="default_menu_box_option">
			<!-- <div id="open_write_twitter" class="menu_box_option">写微博</div> -->
			<div id="open_hf_twitter" class="menu_box_option">回复</div>
			<div id="open_zf_twitter" class="menu_box_option">转发</div>
			<div id="open_yw" class="menu_box_option">详情</div>
			<div id="open_zz" class="menu_box_option">作者</div>
			<div id="digit" class="menu_box_option">赞</div>
		</div>
		<div id="more_menu_box_option">
			<div id="open_more" class="menu_box_option">更多</div>
			<!-- <div id="open_menu" class="menu_box_option">菜单</div> -->
			<div id="open_square" class="menu_box_option">广场</div>
			<div id="back2_default_menu_box_option" class="menu_box_option">返回</div>
		</div>
	</div>
	<!-- 微博输入框 -->
	<!-- //发布微博限制字数 -->
	<?php $admin_Config = model('Xdata')->lget('admin_Config');
		$weibo_nums = $admin_Config['feed']['weibo_nums']; ?>
	<div id="post_twitter">
		<div class="header">
			<div id="post_tip" class="logo_post">发表微博</div>
			<!-- <input type="button" id="post_twitter_button_file" value="附件" tabindex="6"> -->
			<div id="post_twitter_button_file"></div>
			<div id="ifSorC">
				<div id="ifShareFeed" class="no_check">转发到微博</div>
				<div id="ifAsComment" class="no_check">评论给原作者</div>
			</div>
		</div>
	   	<div id="ptibox" nums="<?php echo ($weibo_nums); ?>">
			<form id="post_form" type="fb" action="<?php echo U('w3g/Index/doComment');?>" method="post" enctype="multipart/form-data" ava="<?php echo ($profile); ?>"/>
				<input type="hidden" name="feed_id" value="<?php echo ($_GET['feed_id']); ?>" />
	   			<input type="hidden" name="comment_id" value="<?php echo ($_GET['comment_id']); ?>"/>
				<textarea name="content" id="post_twitter_input"></textarea>
			</form>
	   	</div>
   		<div id="file_list"></div>
		<div id="post_twitter_button">
			<input type="button" id="close_post_twitter" value="取消" tabindex="6">
			<input type="button" id="post_twitter_button_submit" value="发表" tabindex="6">
		</div>
	</div>
	<!-- 发表私信 -->
	<div id="post_message" style="display:none;">
		<div class="header">
			<div id="post_message_tip" class="logo_post">发送私信</div>
		</div>
	   	<div id="pmbox">
			<!-- <div id="post_message_tip">发送私信：</div> -->
				<input id="post_msg_to" type="text" placeholder="发送给"></input>
				<div id="pm2l"></div>
				<textarea name="content" id="post_message_input" placeholder="私信内容"></textarea>
	   	</div>
			<div id="post_message_button">
				<input type="button" id="close_post_message" value="取消" tabindex="6">
				<input type="button" id="post_message_button_submit" value="发表" tabindex="6">
			</div>
	</div>
	<!-- 收藏提示 -->
	<div id="favorited"></div>
	<!-- 右上角菜单 -->
	<div id="sys_menu" class="sys_menu_normal">
		<div id="sys_menu_top"></div>
		<div id="sys_menu_wtire" class="sys_menu_option">写微博</div>
		<div id="sys_menu_i" class="sys_menu_option">我的主页</div>
		<div id="sys_menu_msg" class="sys_menu_option" linkto="at">消息盒子</div>
		<div id="sys_menu_square" class="sys_menu_option">微博广场</div>
		<div id="sys_menu_sendmsg" class="sys_menu_option">发私信</div>
		<div id="sys_menu_search" class="sys_menu_option">搜索</div>
		<div id="sys_menu_quit" class="sys_menu_option">退出</div>
	</div>
	<!-- 局部遮罩 -->
	<div id="part_shadow"></div>
	<!-- 全局遮罩 -->
	<div id="shadow"></div>
	<div id="no"></div>
	<div id="tip">
		<img id="tip_load" src="img/link2load.gif">
		<p id="tip_p" class="tip_p_fl">
			Here is ThinkSNS-3G tips:)
		</p>
		<div id="tip_ik">
			<p id="tip_ikp">
				我知道了
			</p>
		</div>
	</div>
	<div id="tip_shadow"></div>
	<!-- 头部顶栏 -->
	<div id="header">
		<div id="logo"
			 <?php if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE 6.0') !== false): ?>style="_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo ($site["logo"]); ?>', sizingMethod='crop');_background:none;"<?php else: ?>style="background:url(<?php echo ($site["logo"]); ?>) no-repeat;"<?php endif; ?>
		></div>
		<div id="menu_button" class="menu_button_normal"></div>
	</div>
	<!-- 加载提示 -->
	<div id="load_tip">
		<img id="load_tip_img" src="img/load_tip.gif">
	</div>
	<!-- 查看图片大图 -->
	<div id="feed_img_view">
		<div id="feed_img_view_bar">
			<div id="feed_img_view_close"></div>
			<div id="feed_img_view_zan"></div>
		</div>
		<div id="feed_img_view_img_box">
		</div>
		<div id="feed_img_view_bg"></div>
	</div>
	<!-- 用户消息提醒 -->
	<div id="msg_tip_box">
		<div id="msg_tip">
			<p id="msg_tip_p">0</p>
			<img id="msg_tip_img" src="img/msg_tip.png">
		</div>
		<div id="msg_tip_shadow"></div>
	</div>
	<!-- 搜索弹出层  -->
<div id="search_box">
	<div class="header">
		<div id="post_message_tip" class="logo_post" style="height: 20px;">搜索</div>
	</div>
	<div id="search_sort">
		<div id="sscw" class="ssc ssc_check" stype="weibo">微博</div>
		<div id="sscu" class="ssc" stype="user">用户</div>
		<div id="ssct" class="ssc" stype="">话题</div>
	</div>
	<div id="search_input_box">
		<input type="text" name="sibi" id="sibi">
	</div>
	<div id="search_button">
		<input type="button" id="close_search_box" value="取消" tabindex="6">
		<input type="button" id="post_search_submit" value="搜索" tabindex="6">
	</div>
</div>
<div id="refresh_list">
	<p id="refresh_list_p">下拉可以刷新</p>
</div>
<div id="content" interface="detail">
    <!-- 代码块（1） -->
    <div class="cob">
        <div id="c_<?php echo ($weibo["feed_id"]); ?>" isdig="<?php echo ($weibo["is_digg"]); ?>" class="c" cid="<?php echo ($weibo["feed_id"]); ?>" rowid="<?php echo ($weibo["feed_id"]); ?>" appid="<?php echo ($weibo["uid"]); ?>" page="xq_" feedtype="<?php echo ($weibo["type"]); ?>" isdel="<?php echo ($weibo["api_source"]["is_del"]); ?>">
            <!-- 判断是否被收藏 -->
            <?php if($weibo['iscoll']['colled']==1){ ?>
                <div class="sc sc_1" id="sc_<?php echo ($weibo["feed_id"]); ?>" cid="<?php echo ($weibo["feed_id"]); ?>" type="feed"></div>
            <?php }else{ ?>
                <div class="sc" id="sc_<?php echo ($weibo["feed_id"]); ?>" cid="<?php echo ($weibo["feed_id"]); ?>" type="feed"></div>
            <?php } ?>

            <div class="c_info">
                <div class="c_ava">
                    <img src="<?php echo ($weibo['avatar_small']); ?>" width=40 height=40>
                </div>
                <div class="info_text">
                    <div id="c_info_name_<?php echo ($weibo["feed_id"]); ?>" class="c_info_name"><?php echo ($weibo['uname']); ?></div>
                    <div class="c_info_more_box">
                        <div class="c_time"><?php echo (friendlydate($weibo["publish_time"])); ?></div>
                        <div class="c_digg_count">赞：<?php echo ($weibo["digg_count"]); ?></div>
                        <div class="c_zf_count">转发：<?php echo ($weibo["repost_count"]); ?></div>
                        <div class="c_comment_count">评论：<?php echo ($weibo["comment_count"]); ?></div>
                    </div>
                </div>
            </div>
            <div id="c_content_<?php echo ($weibo["feed_id"]); ?>" class="c_content">
                <?php echo $weibo['content']; ?>
            </div>
            <!-- 这一段是显示微吧发的帖子 -->
            <?php if ($weibo['type'] == 'weiba_post'){ ?>
                <div class="c_content">
                    <?php echo '【帖子内容】'.wapFormatContent($weibo['api_source']['content']); ?>
                </div>
            <?php } ?>
            <!-- 原微博的图 -->
            <?php if($weibo['feedType']=='postimage') { ?>
                <div id="feed_img_box_<?php echo ($weibo[feed_id]); ?>" class="feed_img_box">
                    <?php foreach($weibo['attach'] as $value){ ?>
                        <img bm="<?php echo ($value['attach_middle']); ?>" class="feed_img" src="<?php echo ($value['attach_small']); ?>" linkto="<?php echo U('w3g/Index/detail',array('weibo_id'=>$weibo['weibo_id']));?>">
                    <?php } ?>
                </div>
            <?php } ?>
            <!-- 转发的原文内容 开始 -->
            <!-- 转发 -->
            <?php if (($weibo['type'] == 'repost' && $weibo['api_source']['is_del'] == '0') || ($weibo['type'] == 'repost' && $weibo['transpond_data']['api_source']['is_del'] == '0')) { ?>
                <div id="c_zf_box_<?php echo ($weibo["feed_id"]); ?>" class="c_zf_box c_zf_box_detail" data-original-id="<?php echo ($weibo[app_row_id]); ?>">
                    <dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
                    <div class="c_zf_content">
                        <span class="c_zf_content_name">@<?php echo ($weibo["transpond_data"]["uname"]); ?></span>:
                        <?php echo wapFormatContent($weibo['transpond_data'][feed_content]);
                            if($weibo['transpond_data']['type'] == 'weiba_post'){ ?>
                                    <div class="loadweiba" weibaid="<?php echo ($weibo["transpond_data"]["api_source"]["post_id"]); ?>">
                                        查看全文
                                    </div>
                        <?php } ?>
                    </div>
                <!-- 转发的图 -->
                <?php if($weibo['transpond_data']['type']=='postimage') { ?>
                    <div class="feed_img_box">
                        <?php foreach($weibo['transpond_data']['attach'] as $value){ ?>
                            <img bm="<?php echo ($value['attach_middle']); ?>" class="feed_img" src="<?php echo ($value['attach_small']); ?>" linkto="<?php echo U('w3g/Index/detail',array('weibo_id'=>$weibo['weibo_id']));?>">
                        <?php } ?>
                    </div>
                <?php } ?>
                    <div class="c_zf_info">
                        <div class="c_zf_info_time"><?php echo (friendlydate($weibo["transpond_data"]["publish_time"])); ?></div>
                        <div class="c_zf_info_counts">
                            <span class="c_zf_info_count">转发:<?php echo ($weibo["transpond_data"]["repost_count"]); ?></span>
                            <span class="c_zf_info_count">评论:<?php echo ($weibo["transpond_data"]["comment_count"]); ?></span>
                            <span class="c_zf_info_count">赞:<?php echo ($weibo["transpond_data"]["digg_count"]); ?></span>
                        </div>
                    </div>
                    <!-- 附件 -->
                    <div>
                        <?php if(isset($weibo['transpond_data']['attach']) && $weibo['transpond_data']['type'] == 'postfile'){ ?>
                            <div class="attach_box">
                                <?php foreach($weibo['transpond_data']['attach'] as $value){
                                        switch($value['extension']){
                                            case 'png':$type='pic';break;
                                            case 'jpg':$type='pic';break;
                                            case 'jpeg':$type='pic';break;
                                            case 'bmp':$type='pic';break;
                                            case 'gif':$type='pic';break;
                                            case 'zip':$type='zip';break;
                                            case 'rar':$type='zip';break;
                                            case 'doc':$type='word';break;
                                            case 'docx':$type='word';break;
                                            case 'xls':$type='xls';break;
                                            case 'ppt':$type='ppt';break;
                                            case 'pdf':$type='pdf';break;
                                            default:$type='unknow';break;
                                        }
                                        $size=$value['size'];
                                        if($size > 1024 && $size< 1024*1024){
                                            $size=$size/1024;
                                            $size=round($size,2);
                                            $size.='K';
                                        }else if($size < 1024){
                                            $size.='B';
                                        }else{
                                            $size=$size/1024/1024;
                                            $size=round($size,2);
                                            $size.='M';
                                        } ?>
                                    <a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
                                        <div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
                                            <div class="attachs_name">
                                                <?php echo $value['attach_name']; ?>
                                            </div>
                                            <div class="attachs_size">
                                                <?php echo $size; ?>
                                            </div>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php }elseif (($weibo['type'] == 'repost' && $weibo['api_source']['is_del'] == '1') || ($weibo['type'] == 'repost' && $weibo['transpond_data']['api_source']['is_del'] == '1')) { ?>
                <!-- 原微博已被删除 -->
                <div class="c_zf_box">
                    <dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
                    <div class="c_zf_content">原文已被删除</div>
                </div>
            <?php } ?>
            <div>
                <?php if(isset($weibo['attach']) && $weibo['type'] == 'postfile'){ ?>
                    <div class="attach_box">
                        <?php foreach($weibo['attach'] as $value){
                                switch($value['extension']){
                                    case 'png':$type='pic';break;
                                    case 'jpg':$type='pic';break;
                                    case 'jpeg':$type='pic';break;
                                    case 'bmp':$type='pic';break;
                                    case 'gif':$type='pic';break;
                                    case 'zip':$type='zip';break;
                                    case 'rar':$type='zip';break;
                                    case 'doc':$type='word';break;
                                    case 'docx':$type='word';break;
                                    case 'xls':$type='xls';break;
                                    case 'ppt':$type='ppt';break;
                                    case 'pdf':$type='pdf';break;
                                    default:$type='unknow';break;
                                }
                                $size=$value['size'];
                                if($size > 1024 && $size< 1024*1024){
                                    $size=$size/1024;
                                    $size=round($size,2);
                                    $size.='K';
                                }else if($size < 1024){
                                    $size.='B';
                                }else{
                                    $size=$size/1024/1024;
                                    $size=round($size,2);
                                    $size.='M';
                                } ?>
                            <a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
                                <div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
                                    <div class="attachs_name">
                                        <?php echo $value['attach_name']; ?>
                                    </div>
                                    <div class="attachs_size">
                                        <?php echo $size; ?>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <!-- 转发的原文内容 结束 -->

        <!-- 微吧转发 -->
            <?php if ($weibo['type'] == 'weiba_repost' && $weibo['api_source']['is_del'] == '0') { ?>
            <div id="c_zf_box_<?php echo ($weibo["feed_id"]); ?>" class="c_zf_box">
                <dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
                <div class="c_zf_content">
                    <span class="c_zf_content_name">@<?php echo ($weibo["api_source"]["source_user_info"]["uname"]); ?></span>:
                    <?php echo wapFormatContent($weibo['api_source']['source_content']);
                        if($weibo['type'] == 'weiba_repost'){ ?>
                            <div class="loadweiba" weibaid="<?php echo ($weibo["api_source"]["post_id"]); ?>">
                                查看全文
                            </div>
                        <?php } ?>
                </div>
                <div class="c_zf_info">
                    <div class="c_zf_info_time"><?php echo (friendlydate($weibo["transpond_data"]["publish_time"])); ?></div>
                    <div class="c_zf_info_counts">
                        <span class="c_zf_info_count">回复:<?php echo ($weibo["api_source"]["reply_count"]); ?></span>
                        <span class="c_zf_info_count">浏览:<?php echo ($weibo["api_source"]["read_count"]); ?></span>
                    </div>
                </div>
            </div>
            <?php }elseif($weibo['type'] == 'weiba_repost' && $weibo['api_source']['is_del'] == '1'){ ?>
                <!-- 原微博已被删除 -->
                <div class="c_zf_box">
                    <dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
                    <div class="c_zf_content">原文已被删除</div>
                </div>
            <?php } ?>
        </div>
        <div class="c_d">
            <div class="c_d_yes">
                <div class="c_d_text">
                    删除
                </div>
            </div>
            <div class="c_d_no">
                <div class="c_d_text">
                    取消
                </div>
            </div>
        </div>
    </div><!--.cob end-->
    <!-- 代码块(1) end -->
    <!-- 评论回复框 -->
    <div id="c_comment_post_box" weibo_id="<?php echo ($weibo['feed_id']); ?>" appid="<?php echo ($weibo['uid']); ?>">
        我来说两句...
    </div>
    <!-- 评论列表 -->
    <div id="c_comment_box">
        <!-- 循环开始 -->
        <?php if(is_array($comment)): ?><?php $i = 0;?><?php $__LIST__ = $comment?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$vo): ?><?php ++$i;?><?php $mod = ($i % 2 )?><div class="c_comments" touid="<?php echo ($vo["uid"]); ?>" rowid="<?php echo ($vo["row_id"]); ?>" appuid="<?php echo ($vo["app_uid"]); ?>" ccid="<?php echo ($vo["comment_id"]); ?>">
            <!--这里的ccid为评论id  appuid为微博详情页原微博作者的ID rowid为原微博ID touid为原评论者的ID  -->
            <div class="c_comments_ava">
                <a href="#">
                    <img src="<?php echo ($vo['user_info']['avatar_small']); ?>" width=30 height=30>
                </a>
            </div>
            <div class="c_comments_content">
                <div class="c_comments_content_info">
                    <div class="c_comments_content_info_name" id="cc_name_<?php echo ($vo["comment_id"]); ?>"><?php echo ($vo['user_info']['uname']); ?></div>
                    <div class="c_comments_content_info_time">
                        <?php $ctime=strtotime($vo[ctime]) ?><?php echo (friendlydate($ctime)); ?>
                    </div>
                </div>
                <p class="c_comments_content_p">
                    <?php echo wapFormatContent($vo['content']); ?>
                </p>
            </div>
        </div><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>
        <!-- 循环结束 -->
        <!-- 分页 -->
        <?php $weibo[comment_count]==0?$count=1:$count=$weibo[comment_count]; ?>
        <div id="pager_box">
            <?php if(empty($_GET['page']) || $_GET['page']==1){ ?>
                <a href="javascript:;" link="" id="prev" class="pager_child pager_p_n_none ">上一页</a>
            <?php }else{ ?>
                <a href="javascript:;" link="<?php echo U('w3g/Index/detail',array('weibo_id'=>$_GET['weibo_id'],'page'=>$page-1));?>" id="prev" class="pager_child pager_p_n ">上一页</a>
            <?php } ?>
            <select id="page_sel" class="pager_child">
                <?php $_GET['page']!=0?$get=$_GET['page']:$get=1;
                    $_count = ceil($count/10);
                    if($get<6){
                        for($i=0;$i<($_count);$i++){
                            $get==$i+1?$selected='selected="selected"':$selected='';
                            if($i>9){
                                break;
                            } ?>
                <option value="<?php echo U('w3g/Index/detail');?>&page=<?php echo ($i+1); ?>&weibo_id=<?php echo ($_GET['weibo_id']); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
                <?php }
                    }elseif($get>=6){
                        for($i=($get-5);$i<$get;$i++){
                            $get==$i+1?$selected='selected="selected"':$selected=''; ?>
                <option value="<?php echo U('w3g/Index/detail');?>&page=<?php echo ($i+1); ?>&weibo_id=<?php echo ($_GET['weibo_id']); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
                <?php } ?>
                <?php for($i=$get;$i<($get+5);$i++){
                            $get==$i+1?$selected='selected="selected"':$selected='';
                            if($i>$_count-1){
                                break;
                            } ?>
                <option value="<?php echo U('w3g/Index/detail');?>&page=<?php echo ($i+1); ?>&weibo_id=<?php echo ($_GET['weibo_id']); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
                <?php } ?>
                <?php } ?>
                
            </select>
            <?php if($get == ($_count)) { ?>
                <a href="javascript:;" link="" id="next" class="pager_child pager_p_n_none">下一页</a>
            <?php }else{ ?>
                <a href="javascript:;" link="<?php echo U('w3g/Index/detail',array('weibo_id'=>$_GET['weibo_id'],'page'=>$page+1));?>" id="next" class="pager_child pager_p_n">下一页</a>
            <?php } ?>
        </div>
    </div>
	<!-- footer -->
	<div id="footer">
		<p id="footer_p">©ThinkSNS.com  2013</p>
	</div>
</div>
</body>
</html>