<?php if (!defined('THINK_PATH')) exit();?><feed app='public' type='repost' info='转发微博'>
	<title> 
		<![CDATA[<?php echo ($actor); ?>]]>
	</title>
	<body>
		<![CDATA[
		<?php if(($body)  ==  ""): ?>微博分享<?php endif; ?> 
		<?php echo (replaceurl(t($body))); ?>
		<dl class="comment">
			<dt class="arrow bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
			<?php if($sourceInfo['is_del'] == 0 && $sourceInfo['source_user_info'] != false): ?>
			<dd class="name">
				<?php echo getUserSpace($sourceInfo["source_user_info"]["uid"],'','','@{uname}') ?>
			</dd>
			<dd>
				<?php echo constant(" 转发原文 *");?>
				<?php echo (replaceurl(t($sourceInfo["source_content"]))); ?>
				<?php if(!empty($sourceInfo['attach'])): ?>
				<?php echo constant(" 附件微博 *");?>
				<?php if(($sourceInfo["feedType"])  ==  "postfile"): ?><ul class="feed_file_list">
					<?php if(is_array($sourceInfo["attach"])): ?><?php $i = 0;?><?php $__LIST__ = $sourceInfo["attach"]?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$vo): ?><?php ++$i;?><?php $mod = ($i % 2 )?><li>
						<a href="<?php echo U('widget/Upload/down',array('attach_id'=>$vo['attach_id']));?>" class="current right" target="_blank"><i class="ico-down"></i></a>
						<i class="ico-<?php echo ($vo["extension"]); ?>-small"></i>
						<a href="<?php echo U('widget/Upload/down',array('attach_id'=>$vo['attach_id']));?>"><?php echo ($vo["attach_name"]); ?></a>
						<span class="tips">(<?php echo (byte_format($vo["size"])); ?>)</span>
					</li><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>			
				</ul><?php endif; ?>
				<?php echo constant(" 图片微博 *");?>
				<?php if(($sourceInfo["feedType"])  ==  "postimage"): ?><div class="feed_img_lists" rel='small' >
					<ul class="small">
						<?php if(is_array($sourceInfo["attach"])): ?><?php $i = 0;?><?php $__LIST__ = $sourceInfo["attach"]?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$vo): ?><?php ++$i;?><?php $mod = ($i % 2 )?><?php if(count($sourceInfo['attach']) == 1): ?>
						<li><a href="javascript:void(0)" event-node="img_small"><img class="imgicon" src='<?php echo ($vo["attach_small"]); ?>' title='点击放大' width="100" height="100"></a></li>
						<?php else: ?>
						<li><a href="javascript:void(0)" onclick="core.weibo.showBigImage(<?php echo ($sourceInfo['feed_id']); ?>, <?php echo ($i); ?>);"><img class="imgicon" src='<?php echo ($vo["attach_small"]); ?>' title='点击放大' width="100" height="100"></a></li>
						<?php endif; ?><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>
					</ul>
				</div>
				<div class="feed_img_lists" rel='big' style='display:none'>
					<ul class="feed_img_list big">
						<span class='tools'>
							<a href="javascript:void(0);" event-node='img_big'><i class="ico-pack-up"></i>收起</a>
							<a target="_blank" href="<?php echo ($vo["attach_url"]); ?>"><i class="ico-show-big"></i>查看大图</a>
							<a href="javascript:;" onclick="revolving('left', <?php echo ($sourceInfo['feed_id']); ?>)"><i class="ico-turn-l"></i>向左转</a>
							<a href="javascript:;" onclick="revolving('right', <?php echo ($sourceInfo['feed_id']); ?>)"><i class="ico-turn-r"></i>向右转</a>
						</span>
						<?php if(is_array($sourceInfo["attach"])): ?><?php $i = 0;?><?php $__LIST__ = $sourceInfo["attach"]?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$vo): ?><?php ++$i;?><?php $mod = ($i % 2 )?><li title='<?php echo ($vo["attach_url"]); ?>'>
							<a href="javascript:void(0)" event-node='img_big'><img id="image_index_<?php echo ($sourceInfo['feed_id']); ?>" maxwidth="550" class="imgsmall" src='<?php echo ($vo["attach_middle"]); ?>' title='点击缩小' /></a>
						</li><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>
					</ul>
				</div><?php endif; ?>
				<?php endif; ?>
				<?php echo constant(" 视频微博 *");?>
				<?php if(($sourceInfo["feedType"])  ==  "postvideo"): ?><div class="feed_img" id="video_mini_show_<?php echo ($feedid); ?>">
				  <a href="javascript:void(0);" onclick="switchVideo(<?php echo ($feedid); ?>,'open','<?php echo ($sourceInfo["host"]); ?>','<?php echo ($sourceInfo["flashvar"]); ?>')">
				    <img src="<?php echo ($sourceInfo["flashimg"]); ?>" style="width:150px;height:113px;overflow:hidden" />
				  </a>
				  <div class="video_play" ><a href="javascript:void(0);" onclick="switchVideo(<?php echo ($feedid); ?>,'open','<?php echo ($sourceInfo["host"]); ?>','<?php echo ($sourceInfo["flashvar"]); ?>')">
				      <img src="__THEME__/image/feedvideoplay.gif" ></a>
				  </div>
				</div>
				<div class="feed_quote" style="display:none;" id="video_show_<?php echo ($feedid); ?>"> 
				  <div class="q_tit">
				    <img class="q_tit_l" onclick="switchVideo(<?php echo ($feedid); ?>,'open','<?php echo ($sourceInfo["host"]); ?>','<?php echo ($sourceInfo["flashvar"]); ?>')" src="__THEME__/image/zw_img.gif" />
				  </div>
				  <div class="q_con"> 
				    <p style="margin:0;margin-bottom:5px" class="cGray2 f12">
				    <a href="javascript:void(0)" onclick="switchVideo(<?php echo ($feedid); ?>,'close')"><i class="ico-pack-up"></i>收起</a>
				    &nbsp;&nbsp;|&nbsp;&nbsp;
				    <a href="<?php echo ($sourceInfo["source"]); ?>" target="_blank">
				      <i class="ico-show-all"></i><?php echo ($sourceInfo["title"]); ?></a>
				    </p>
				    <div id="video_content_<?php echo ($feedid); ?>"></div>
				  </div>
				  <div class="q_btm"><img class="q_btm_l" src="__THEME__/image/zw_img.gif" /></div>
				</div><?php endif; ?>
			</dd>
			<p class="info">
				<span class="right">
					<a href="<?php echo U('public/Profile/feed',array('uid'=>$sourceInfo['uid'],'feed_id'=>$sourceInfo['feed_id']));?>">原文转发<?php if(($sourceInfo["repost_count"])  !=  "0"): ?>(<?php echo ($sourceInfo["repost_count"]); ?>)<?php endif; ?></a><i class="vline">|</i>
					<a href="<?php echo U('public/Profile/feed',array('uid'=>$sourceInfo['uid'],'feed_id'=>$sourceInfo['feed_id']));?>">原文评论<?php if(($sourceInfo["comment_count"])  !=  "0"): ?>(<?php echo ($sourceInfo["comment_count"]); ?>)<?php endif; ?></a>
				</span>
				<span><a href="<?php echo U('public/Profile/feed',array('uid'=>$sourceInfo['uid'],'feed_id'=>$sourceInfo['feed_id']));?>" class="date" date="<?php echo ($sourceInfo['publish_time']); ?>"><?php echo (friendlydate($sourceInfo['publish_time'])); ?></a><span><?php echo getFromClient($sourceInfo['from']);?></span></span>
			</p>
			<?php else: ?>
			<dd class="name">内容已被删除</dd>
			<?php endif; ?>
		</dl>
		]]>
	</body>
	<feedAttr comment="true" repost="true" like="false" favor="true" delete="true" />
</feed>