<?php if (!defined('THINK_PATH')) exit();?><!-- 循环开始 -->
<?php if(is_array($comment)): ?><?php $i = 0;?><?php $__LIST__ = $comment?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$vo): ?><?php ++$i;?><?php $mod = ($i % 2 )?><div class="c_comments" touid="<?php echo ($vo["uid"]); ?>" rowid="<?php echo ($vo["row_id"]); ?>" appuid="<?php echo ($vo["app_uid"]); ?>" ccid="<?php echo ($vo["comment_id"]); ?>">
    <!--这里的ccid为评论id  appuid为微博详情页原微博作者的ID rowid为原微博ID touid为原评论者的ID  -->
    <div class="c_comments_ava">
        <a href="#">
            <img src="<?php echo ($vo['user_info']['avatar_small']); ?>" width=30 height=30>
        </a>
    </div>
    <div class="c_comments_content">
        <div class="c_comments_content_info">
            <div class="c_comments_content_info_name"  id="cc_name_<?php echo ($vo["comment_id"]); ?>">
                <?php echo ($vo['user_info']['uname']); ?>
            </div>
            <div class="c_comments_content_info_time">
                <?php $ctime=strtotime($vo[ctime]) ?><?php echo (friendlydate($ctime)); ?>
            </div>
        </div>
        <p class="c_comments_content_p">
            <?php echo wapFormatContent($vo['content']); ?>
        </p>
    </div>
</div><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>
<!-- 循环结束 -->
<!-- 分页 -->
        <?php $count=$weibo['comment_count']==0?$count=1:$count=$weibo['comment_count']; ?>
        <div id="pager_box">
            <?php if(empty($_GET['page']) || $_GET['page']==1){ ?>
                <a href="javascript:;" link="" id="prev" class="pager_child pager_p_n_none ">上一页</a>
            <?php }else{ ?>
                <a href="javascript:;" link="<?php echo U('w3g/Index/detail',array('weibo_id'=>$_GET['weibo_id'],'page'=>$page-1));?>" id="prev" class="pager_child pager_p_n ">上一页</a>
            <?php } ?>
            <select id="page_sel" class="pager_child">
                <?php $_GET['page']!=0?$get=$_GET['page']:$get=1;
                    $_count = ceil($count/10);
                    if($get<6){
                        for($i=0;$i<($_count);$i++){
                            $get==$i+1?$selected='selected="selected"':$selected='';
                            if($i>9){
                                break;
                            } ?>
                <option value="<?php echo U('w3g/Index/detail');?>&page=<?php echo ($i+1); ?>&weibo_id=<?php echo ($_GET['weibo_id']); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
                <?php }
                    }elseif($get>=6){
                        for($i=($get-5);$i<$get;$i++){
                            $get==$i+1?$selected='selected="selected"':$selected=''; ?>
                <option value="<?php echo U('w3g/Index/detail');?>&page=<?php echo ($i+1); ?>&weibo_id=<?php echo ($_GET['weibo_id']); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
                <?php } ?>
                <?php for($i=$get;$i<($get+5);$i++){
                            $get==$i+1?$selected='selected="selected"':$selected='';
                            if($i>$_count-1){
                                break;
                            } ?>
                <option value="<?php echo U('w3g/Index/detail');?>&page=<?php echo ($i+1); ?>&weibo_id=<?php echo ($_GET['weibo_id']); ?>" <?php echo $selected; ?>>第<?php echo ($i+1); ?>页</option>
                <?php } ?>
                <?php } ?>
                
            </select>
            <?php if($get == ($_count)) { ?>
                <a href="javascript:;" link="" id="next" class="pager_child pager_p_n_none">下一页</a>
            <?php }else{ ?>
                <a href="javascript:;" link="<?php echo U('w3g/Index/detail',array('weibo_id'=>$_GET['weibo_id'],'page'=>$page+1));?>" id="next" class="pager_child pager_p_n">下一页</a>
            <?php } ?>
        </div>
    </div>