<?php if (!defined('THINK_PATH')) exit();?><div class="extend-foot">
  <div class="feed_lists clearfix" id="feed-lists">
    <?php $cancomment=intval(CheckPermission('core_normal','feed_comment')); ?>
    <?php if(is_array($list["data"])): ?><?php $i = 0;?><?php $__LIST__ = $list["data"]?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$vl): ?><?php ++$i;?><?php $mod = ($i % 2 )?><?php $cancomment_old=empty($vl['app_row_id']) ? 0 : 1; ?>
      <dl class="feed_list" id="feed<?php echo ($vl["feed_id"]); ?>" model-node="feed_list">
        <dt class="face">
          <a href="<?php echo ($vl['user_info']['space_url']); ?>"><img src="<?php echo ($vl['user_info']['avatar_small']); ?>"  event-node="face_card" uid='<?php echo ($vl['user_info']['uid']); ?>'></a>
        </dt>
        <dd class="content">
        <span event-node="show_admin" event-args="feed_id=<?php echo ($vl['feed_id']); ?>&channel_id=<?php echo ($cid); ?>&clear=1&uid=<?php echo ($vl['user_info']['uid']); ?>&feed_del=<?php echo CheckPermission('core_admin','feed_del');?>&channel_recommend=<?php echo CheckPermission('channel_admin','channel_recommend');?>" href="javascript:;" class="right f12 f9 hover" style="display:none;cursor:pointer">管理</span>
        <?php if(($vl["is_del"])  ==  "0"): ?><p class="hd"><?php echo getUserSpace($vl["user_info"]["uid"],'','','{uname}') ?>
        <?php if(in_array($vl['user_info']['uid'],$followUids)): ?>
        <?php echo W('Remark',array('uid'=>$vl['user_info']['uid'],'remark'=>$remarkHash[$vl['user_info']['uid']],'showonly'=>1));?>
        <?php endif; ?>
        </p>
        <span class="contents"><?php echo (format($vl["body"],true)); ?></span>

        <p class="info">
          <span class="right">
            <?php if(in_array('repost',$weibo_premission) && $vl['actions']['repost'] && CheckPermission('core_normal','feed_share')): ?>
            <?php $sid=!empty($vl['app_row_id']) ? $vl['app_row_id'] : $vl['feed_id'];$cancomment_old = in_array($vl['type'],$cancomment_old_type) ? 1 : 0; ?>
            <?php echo W('Share',array('sid'=>$sid,'stable'=>$vl['app_row_table'],'initHTML'=>'','current_table'=>'feed','current_id'=>$vl['feed_id'],'nums'=>$vl['repost_count'],'appname'=>$vl['app'],'cancomment'=>$cancomment_old,'feed_type'=>$vl['type'],'is_repost'=>$vl['is_repost']));?>
            <i class="vline">|</i>
            <?php endif; ?>

            <?php if(($vl["actions"]["favor"])  ==  "true"): ?><?php echo W('Collection',array('type'=>$type,'sid'=>$vl['feed_id'],'stable'=>'feed','sapp'=>$vl['app']));?><?php endif; ?>

            <?php if(in_array('comment',$weibo_premission) && $vl['actions']['comment']): ?>
            <i class="vline">|</i>
            <a event-node="comment" href="javascript:void(0)" event-args='row_id=<?php echo ($vl["feed_id"]); ?>&app_uid=<?php echo ($vl["uid"]); ?>&app_row_id=<?php echo ($vl["app_row_id"]); ?>&app_row_table=<?php echo ($vl["app_row_table"]); ?>&to_comment_id=0&to_uid=0&app_name=<?php echo ($vl["app"]); ?>&table=feed&cancomment=<?php echo ($cancomment); ?>&cancomment_old=<?php echo ($cancomment_old); ?>'><?php echo L('PUBLIC_STREAM_COMMENT');?><?php if(($vl["comment_count"])  !=  "0"): ?>(<?php echo ($vl["comment_count"]); ?>)<?php endif; ?></a>
            <?php endif; ?>
          </span>
          <span>
            <a class="date" href="<?php echo U('public/Profile/feed',array('feed_id'=>$vl['feed_id'],'uid'=>$vl['uid']));?>"><?php echo (friendlydate($vl["publish_time"])); ?></a>
            <span><?php echo (getfromclient($vl['from'])); ?></span>

            <em class="hover">
            <?php if($vl['actions']['delete'] && $vl['user_info']['uid'] == $GLOBALS['ts']['mid']): ?>
            <a href="javascript:void(0)" event-node ='delFeed' event-args='feed_id=<?php echo ($vl["feed_id"]); ?>&uid=<?php echo ($vl["user_info"]["uid"]); ?>'><?php echo L('PUBLIC_STREAM_DELETE');?></a>
            <?php endif; ?>
            <?php if($vl['user_info']['uid'] != $GLOBALS['ts']['mid'] && CheckPermission('core_normal','feed_report')): ?>
            <a href="javascript:void(0)" event-node='denounce' event-args='aid=<?php echo ($vl["feed_id"]); ?>&type=feed&uid=<?php echo ($vl["user_info"]["uid"]); ?>'><?php echo L('PUBLIC_STREAM_REPORT');?></a>
            <?php endif; ?>
            </em>
          </span>
        </p>
        <div model-node="comment_detail" class="repeat clearfix" style="display:none;"></div>
        <?php else: ?>
        <p><?php echo L('PUBLIC_INFO_ALREADY_DELETE_TIPS');?></p>
        <p class="info">
        <?php if(($vl["actions"]["favor"])  ==  "true"): ?><?php echo W('Collection',array('type'=>$type,'sid'=>$vl['feed_id'],'stable'=>'feed','sapp'=>$vl['app']));?><?php endif; ?>
      </p><?php endif; ?> 
      </dd>
    </dl><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>
    <div class="page"><?php echo ($list["html"]); ?></div>
  </div>
</div>

<script type="text/javascript" src="__APP__/channel.js"></script>
<script type="text/javascript" src="__THEME__/js/module.weibo.js"></script>