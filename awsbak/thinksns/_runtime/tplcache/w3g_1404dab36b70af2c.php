<?php if (!defined('THINK_PATH')) exit();?><?php if(count($weibolist,0)>10){
	echo 'moreThanTen';
}else{ ?>
	<?php if(is_array($weibolist)): ?><?php $i = 0;?><?php $__LIST__ = $weibolist?><?php if( count($__LIST__)==0 ) : echo "" ; ?><?php else: ?><?php foreach($__LIST__ as $key=>$feed): ?><?php ++$i;?><?php $mod = ($i % 2 )?><div class="cob">
	<div class="c" isdig="<?php echo ($feed["is_digg"]); ?>" id="c_<?php echo ($feed["feed_id"]); ?>" cid="<?php echo ($feed["feed_id"]); ?>" rowid="<?php echo ($feed["feed_id"]); ?>" appid="<?php echo ($feed["uid"]); ?>" from="index" page="" feedtype="<?php echo ($feed["type"]); ?>" isdel="<?php echo ($feed["api_source"]["is_del"]); ?>">
		<!-- 判断是否被收藏 -->
		<?php if($feed['favorited']==1){ ?>
			<div class="sc sc_1" id="sc_<?php echo ($feed["feed_id"]); ?>" cid="<?php echo ($feed["feed_id"]); ?>" type="<?php echo ($feed["app_row_table"]); ?>"></div>
		<?php }else{ ?>
			<div class="sc" id="sc_<?php echo ($feed["feed_id"]); ?>" cid="<?php echo ($feed["feed_id"]); ?>" type="<?php echo ($feed["app_row_table"]); ?>"></div>
		<?php } ?>

		<!-- 原微博 -->
		<div class="c_info">
			<div class="c_ava">
				<img src="<?php echo ($feed["avatar_small"]); ?>" width=40 height=40>
			</div>
			<div class="info_text">
				<div id="c_info_name_<?php echo ($feed["feed_id"]); ?>" class="c_info_name"><?php echo ($feed["uname"]); ?></div>
				<div class="c_info_more_box">
					<div id="c_time_<?php echo ($feed["feed_id"]); ?>" class="c_time"><?php echo (friendlydate($feed["publish_time"])); ?></div>
					<div id="c_digg_count_<?php echo ($feed["feed_id"]); ?>" class="c_digg_count">赞：<?php echo ($feed["digg_count"]); ?></div>
					<div id="c_zf_count_<?php echo ($feed["feed_id"]); ?>" class="c_zf_count">转发：<?php echo ($feed["repost_count"]); ?></div>
					<div id="c_comment_count_<?php echo ($feed["feed_id"]); ?>" class="c_comment_count">评论：<?php echo ($feed["comment_count"]); ?></div>
				</div>
			</div>
		</div>
		<div id="c_content_<?php echo ($feed["feed_id"]); ?>" class="c_content">
			<?php echo wapFormatContent($feed[content]);
				if($feed['type'] == 'weiba_post'){ ?>
						<div class="loadweiba" weibaid="<?php echo ($feed["api_source"]["post_id"]); ?>">
							查看全文
						</div>
					<?php } ?>
		</div>

		<!-- 原微博的图 -->
		<?php if($feed['feedType']=='postimage') { ?>
		<div id="feed_img_box_<?php echo ($feed["feed_id"]); ?>" class="feed_img_box">
			<?php foreach($feed['attach'] as $value){ ?>
				<img bm="<?php echo ($value['attach_middle']); ?>" class="feed_img" src="<?php echo ($value['attach_small']); ?>" linkto="<?php echo U('w3g/Index/detail',array('weibo_id'=>$feed['weibo_id']));?>">
			<?php } ?>
		</div>
		<?php } ?>

		<!-- 转发 -->
		<?php if (($feed['type'] == 'repost' && $feed['api_source']['is_del'] == '0') || ($feed['type'] == 'repost' && $feed['transpond_data']['api_source']['is_del'] == '0')) { ?>
		<div id="c_zf_box_<?php echo ($feed["feed_id"]); ?>" class="c_zf_box">
			<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
			<div class="c_zf_content">
				<span class="c_zf_content_name">@<?php echo ($feed["transpond_data"]["uname"]); ?></span>:
				<?php echo wapFormatContent($feed['transpond_data'][feed_content]);
					if($feed['transpond_data']['type'] == 'weiba_post'){ ?>
						<div class="loadweiba" weibaid="<?php echo ($feed["transpond_data"]["api_source"]["post_id"]); ?>">
							查看全文
						</div>
					<?php } ?>
			</div>
		<!-- 转发的图 -->
			<?php if($feed['transpond_data']['type']=='postimage') { ?>
				<div class="feed_img_box">
					<?php foreach($feed['transpond_data']['attach'] as $value){ ?>
						<img bm="<?php echo ($value['attach_middle']); ?>" class="feed_img" src="<?php echo ($value['attach_small']); ?>">
					<?php } ?>
				</div>
			<?php } ?>
			<div class="c_zf_info">
				<div class="c_zf_info_time"><?php echo (friendlydate($feed["transpond_data"]["publish_time"])); ?></div>
				<div class="c_zf_info_counts">
					<span class="c_zf_info_count">转发:<?php echo ($feed["transpond_data"]["repost_count"]); ?></span>
					<span class="c_zf_info_count">评论:<?php echo ($feed["transpond_data"]["comment_count"]); ?></span>
					<span class="c_zf_info_count">赞:<?php echo ($feed["transpond_data"]["digg_count"]); ?></span>
				</div>
			</div>
			<!-- 附件 -->
			<div><?php if(isset($feed['transpond_data']['attach']) && $feed['transpond_data']['type'] == 'postfile'){ ?>
				<div class="attach_box">
					<?php foreach($feed['transpond_data']['attach'] as $value){
							switch($value['extension']){
								case 'png':$type='pic';break;
								case 'jpg':$type='pic';break;
								case 'jpeg':$type='pic';break;
								case 'bmp':$type='pic';break;
								case 'gif':$type='pic';break;
								case 'zip':$type='zip';break;
								case 'rar':$type='zip';break;
								case 'doc':$type='word';break;
								case 'docx':$type='word';break;
								case 'xls':$type='xls';break;
								case 'ppt':$type='ppt';break;
								case 'pdf':$type='pdf';break;
								default:$type='unknow';break;
							}
							$size=$value['size'];
							if($size > 1024 && $size< 1024*1024){
								$size=$size/1024;
								$size=round($size,2);
								$size.='K';
							}else if($size < 1024){
								$size.='B';
							}else{
								$size=$size/1024/1024;
								$size=round($size,2);
								$size.='M';
							} ?>
						<a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
							<div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
								<div class="attachs_name">
									<?php echo $value['attach_name']; ?>
								</div>
								<div class="attachs_size">
									<?php echo $size; ?>
								</div>
							</div>
						</a>
					<?php } ?>
				</div>
			  <?php } ?>
			</div>
		</div>
		<?php }elseif (($feed['type'] == 'repost' && $feed['api_source']['is_del'] == '1') || ($feed['type'] == 'repost' && $feed['transpond_data']['api_source']['is_del'] == '1')) { ?>
			<!-- 原微博已被删除 -->
			<div class="c_zf_box">
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">原文已被删除</div>
			</div>
		<?php } ?>

		<!-- 微吧转发 -->
		<?php if (($feed['type'] == 'weiba_repost' && $feed['api_source']['is_del'] == '0') || ($feed['type'] == 'weiba_repost' && $feed['transpond_data']['is_del'] == '0') || ($feed['type'] == 'weiba_repost' && $feed['api_source']['api_source']['is_del'] == '0')) { ?>
		<div id="c_zf_box_<?php echo ($feed["feed_id"]); ?>" class="c_zf_box">
			<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
			<div class="c_zf_content">
				<span class="c_zf_content_name">@<?php echo ($feed["api_source"]["source_user_info"]["uname"]); ?></span>:
				<?php echo wapFormatContent($feed['api_source']['source_content']); ?>
			</div>
			<div class="c_zf_info">
				<div class="c_zf_info_time"><?php echo (friendlydate($feed[api_source][post_time])); ?></div>
				<div class="c_zf_info_counts">
					<span class="c_zf_info_count">评论:<?php echo ($feed[api_source][reply_count]); ?></span>
				</div>
			</div>
			<div class="loadweiba" weibaid="<?php echo ($feed["api_source"]["post_id"]); ?>">查看全文</div>
		</div>
		<?php }elseif(($feed['type'] == 'weiba_repost' && $feed['api_source']['is_del'] == '1') || ($feed['type'] == 'weiba_repost' && $feed['transpond_data']['is_del'] == '1')) { ?>
			<!-- 原微博已被删除 -->
			<div class="c_zf_box">
				<dt class="bgcolor_arrow"><em class="arrline">◆</em><span class="downline">◆</span></dt>
				<div class="c_zf_content">原文已被删除</div>
			</div>
		<?php } ?>

		<div ><?php if(isset($feed['attach']) && $feed['type'] == 'postfile'){ ?>
					<div class="attach_box">
						<?php foreach($feed['attach'] as $value){
								switch($value['extension']){
									case 'png':$type='pic';break;
									case 'jpg':$type='pic';break;
									case 'jpeg':$type='pic';break;
									case 'bmp':$type='pic';break;
									case 'gif':$type='pic';break;
									case 'zip':$type='zip';break;
									case 'rar':$type='zip';break;
									case 'doc':$type='word';break;
									case 'docx':$type='word';break;
									case 'xls':$type='xls';break;
									case 'ppt':$type='ppt';break;
									case 'pdf':$type='pdf';break;
									default:$type='unknow';break;
								}
								$size=$value['size'];
								if($size > 1024 && $size< 1024*1024){
									$size=$size/1024;
									$size=round($size,2);
									$size.='K';
								}else if($size < 1024){
									$size.='B';
								}else{
									$size=$size/1024/1024;
									$size=round($size,2);
									$size.='M';
								} ?>
							<a class="attachs_a" href="<?php echo U('widget/Upload/down',array('attach_id'=>$value['attach_id']));?>" type="<?php echo $value['extension']; ?>">
								<div class="attachs" style="background-image:url(img/attach/<?php echo ($type); ?>.png)">
									<div class="attachs_name">
										<?php echo $value['attach_name']; ?>
									</div>
									<div class="attachs_size">
										<?php echo $size; ?>
									</div>
								</div>
							</a>
						<?php } ?>
					</div>
			  <?php } ?>
		</div>
	</div>
		<div class="c_d">
			<div class="c_d_yes">
				<div class="c_d_text">
					删除
				</div>
			</div>
			<div class="c_d_no">
				<div class="c_d_text">
					取消
				</div>
			</div>
		</div>
	</div><!--.cob end--><?php endforeach; ?><?php endif; ?><?php else: echo "" ;?><?php endif; ?>
<?php } ?>